import "regent"

local stdlib = terralib.includec("stdlib.h")
local c = regentlib.c
local cstr = terralib.includec("string.h")

fspace input {
  x : double,
  y : double,
}

fspace output {
  z : double,
}

task init(input_lr : region(ispace(int1d), input),
          seed : long)
where writes(input_lr.{x, y}) do
  var buffer: stdlib.drand48_data
  stdlib.srand48_r(seed, &buffer)
  for i in input_lr do
    var x: double, y: double
    stdlib.drand48_r(&buffer, &x)
    stdlib.drand48_r(&buffer, &y)
    input_lr[i].x = x
    input_lr[i].y = y
  end
end

task daxpy(input_lr : region(ispace(int1d), input),
           output_lr : region(ispace(int1d), output),
           num_iterations : int,
           seed : long)
where reads writes(output_lr.z), reads(input_lr.{x, y}) do
  var buffer: stdlib.drand48_data
  stdlib.srand48_r(seed, &buffer)
  for i in output_lr do
    output_lr[i].z = 0.0
  end
  for k = 0, num_iterations do
    for i in input_lr do
      var rn: double
      stdlib.drand48_r(&buffer, &rn)
      output_lr[i].z = rn*input_lr[i].x + input_lr[i].y
    end
  end
end

task main()
  var args = c.legion_runtime_get_input_args()
  var num_elements = 128
  var num_subregions = 4
  var num_iterations = 1
  for i = 0, args.argc do
    if cstr.strcmp(args.argv[i], "-n") == 0 then
      num_elements = stdlib.atoi(args.argv[i+1])
    elseif cstr.strcmp(args.argv[i], "-b") == 0 then
      num_subregions = stdlib.atoi(args.argv[i+1])
    elseif cstr.strcmp(args.argv[i], "-r") == 0 then
      num_iterations = stdlib.atoi(args.argv[i+1])
    end
  end
  var is = ispace(int1d, num_elements)
  var input_lr = region(is, input)
  var output_lr = region(is, output)

  -- Data parallelism in Regent is achieved by partioning a reigon
  -- into subregions. Regent provides a number of partitioning
  -- operators; the code below uses an equal partitioning to create
  -- simple blocked subregions.
  --
  -- The subregions in a partition are also associated with points in
  -- an index space. The ispace ps below names the respective subregions.
  var ps = ispace(int1d, num_subregions)
  var input_lp = partition(equal, input_lr, ps)
  var output_lp = partition(equal, output_lr, ps)

  -- Loops may now call tasks on the subregions of the partitions
  -- declared above. (Note as before the __demand annotation is
  -- optional and causes the compiler to issue an error if the loop
  -- iterations cannot be guarranteed to execute in parallel.)
  __demand(__parallel)
  for i = 0, num_subregions do
    init(input_lp[i], i)
  end

  var alpha = stdlib.drand48()
  __demand(__parallel)
  for i = 0, num_subregions do
    daxpy(input_lp[i], output_lp[i], num_iterations, num_subregions + i)
  end
end
regentlib.start(main)
